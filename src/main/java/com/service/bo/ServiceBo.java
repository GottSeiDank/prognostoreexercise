package com.service.bo;

import java.util.List;

import com.model.Price;

public interface ServiceBo {

	/**
	 * gets the stock prices based on the specified arguments/parameter
	 * @param columnIndex
	 * @param sortOrder
	 * @param startDate
	 * @param endDate
	 * @return a list of {@link Price} objects
	 * @throws Exception
	 */
	public List<Price> getStockPrices(String label, int columnIndex, String sortOrder, String startDate, String endDate) throws Exception;
}
