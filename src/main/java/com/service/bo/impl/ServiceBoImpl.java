package com.service.bo.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.model.Price;
import com.model.dao.PriceDao;
import com.prognostore.util.Utils;
import com.service.bo.ServiceBo;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ServiceBoImpl implements ServiceBo{
	
	PriceDao priceDao;	

	public PriceDao getPriceDao() {
		return priceDao;
	}
	
	public void setPriceDao(PriceDao priceDao) {
		this.priceDao = priceDao;
	}

	/* (non-Javadoc)
	 * @see com.service.bo.ServiceBo#getStockPrices(int, java.lang.String, java.lang.String, java.lang.String)
	 */
	public List<Price> getStockPrices(String label, int columnIndex, String sortOrder, String startDate, String endDate) throws Exception {
		List<Price> prices = new ArrayList<Price>();
		
		try {
			String url = "https://www.quandl.com/api/v3/datasets/WIKI/"+label+"/data.json?column_index="+columnIndex+"&sort_order="+sortOrder+"&start_date="+startDate+"&end_date="+endDate;
			OkHttpClient client = new OkHttpClient();
	    	Request request = new Request.Builder().url(url).get().build();
			Response response = client.newCall(request).execute();
			String r = response.body().string();
			JSONObject result = new JSONObject(r);
			
			if(result.has("quandl_error")){
				throw new Exception(result.getJSONObject("quandl_error").getString("message"));
			}
			JSONArray dataArray = result.getJSONObject("dataset_data").getJSONArray("data");
			
			for(int i = 0; i < dataArray.length(); i++){
				JSONArray array = dataArray.getJSONArray(i);
				Price price = priceDao.save(label, Utils.df.parse(array.getString(0)), new BigDecimal(array.get(1)+""));
				prices.add(price);
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(prices.isEmpty()){
			prices = priceDao.findByDateRange(label, Utils.df.parse(startDate), Utils.df.parse(endDate));
		}
		
		return prices;
	}

}
