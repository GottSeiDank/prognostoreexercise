package com.prognostore;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.Price;
import com.model.RestResponse;
import com.prognostore.util.Utils;
import com.service.bo.ServiceBo;

/**
 * This class impl a restful http for getting stock prices
 * 
 * @author Kaycee-PC
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {

	@Autowired
	ServiceBo serviceBo;

	/**
	 * this method returns stock prices using json data format
	 * 
	 * @return a RestResponse object containing list of stock prices
	 * @author Kaycee-PC
	 */
	@CrossOrigin(origins = "*", maxAge = 3600)
	@RequestMapping(value = "/get-prices", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody RestResponse home(HttpServletRequest request,
			@RequestParam(name = "label", required = true) String label) throws Exception {

		// get the current date
		Calendar now = Calendar.getInstance();

		/*
		 * get the last working days from the current date, if the current day
		 * is friday or saturdaythen the last working days is still in
		 * the same week else the last working days is in the previous week
		 */
		if (now.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY || now.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			now.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		} else {
			now.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
			now.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) - 7);
		}

		Date end = now.getTime();
		now.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) - 4);
		Date start = now.getTime();

		int columnIndex = 4;
		String sortOrder = "asc";
		String startDate = Utils.df.format(start);
		String endDate = Utils.df.format(end);

		List<Price> prices = serviceBo.getStockPrices(label, columnIndex, sortOrder, startDate, endDate);
		return new RestResponse(true, prices, "Request is successful");
	}

	/**
	 * this method is envoke onerror
	 * 
	 * @return a RestResponse object describing the error encountered
	 * @author Kaycee-PC
	 */
	@ExceptionHandler(Exception.class)
	public @ResponseBody RestResponse errorOccured(Exception e) {

		e.printStackTrace();
		return new RestResponse(false, null, e.getMessage());
	}

}
