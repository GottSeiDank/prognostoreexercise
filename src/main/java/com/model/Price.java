package com.model;

import java.math.BigDecimal;
import java.util.Date;

import com.prognostore.util.Utils;

public class Price {
	
	String dateText;
	BigDecimal price;
	Date date;
	
	public Price() {
		super();
	}

	public Price(BigDecimal price, Date date) {
		super();
		this.price = price;
		this.date = date;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDateText() {
		return Utils.df.format(this.date);
	}

	public void setPriceText(String dateText) {
		this.dateText = dateText;
	}	
}
