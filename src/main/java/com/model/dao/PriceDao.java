package com.model.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.model.Price;

public interface PriceDao {
	
	/**
	 * finds a stock price by date
	 * @param date
	 * @return a {@link Price} object
	 * @throws Exception
	 */
	public Price findByDate(String label, Date date)throws Exception;
	
	/**
	 * Save a new price object to embedded database
	 * @param date
	 * @param price
	 * @return a {@link Price} object
	 * @throws Exception
	 */
	public Price save(String label, Date date, BigDecimal price)throws Exception;
	
	/**
	 * finds a stock prices by date range
	 * @param start 
	 * @param end
	 * @return list of {@link Price} objects
	 * @throws Exception
	 */
	public List<Price> findByDateRange(String label, Date start, Date end)throws Exception;
}
