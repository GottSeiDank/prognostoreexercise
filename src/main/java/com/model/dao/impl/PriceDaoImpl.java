package com.model.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.model.Price;
import com.model.dao.PriceDao;

public class PriceDaoImpl implements PriceDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;	

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	/**
	 * @param namedParameterJdbcTemplate
	 * @return void
	 * @author Kaycee-PC
	 */
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	
	/* (non-Javadoc)
	 * @see com.model.dao.PriceDao#findByDate(java.util.Date)
	 */
	public Price findByDate(String label, Date date) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("date", date);
        params.put("label", label);
        
		String sql = "SELECT * FROM price WHERE label=:label AND date=:date";		
        Price result = namedParameterJdbcTemplate.queryForObject(sql,params,new PriceMapper());
		return result;
	}

	/* (non-Javadoc)
	 * @see com.model.dao.PriceDao#save(java.util.Date, java.math.BigDecimal)
	 */
	public Price save(String label, Date date, BigDecimal price) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("date", date);
        params.put("label", label);
        
		String deleteSQL = "DELETE FROM price WHERE date=:date AND label=:label";		
        namedParameterJdbcTemplate.update(deleteSQL,params);
        
		Map<String, Object> params2 = new HashMap<String, Object>();
        params2.put("price", price);
        params2.put("date", date);
        params2.put("label", label);
        
        String insertSQL = "INSERT INTO price VALUES ( :price, :date, :label )";		
        namedParameterJdbcTemplate.execute(insertSQL, params2, new PreparedStatementCallback<Object>() {  
						            public Object doInPreparedStatement(PreparedStatement ps)  
						                    throws SQLException, DataAccessException {  
						                return ps.executeUpdate();  
						            }  
						        });
        
		return new Price(price, date);
	}

	/* (non-Javadoc)
	 * @see com.model.dao.PriceDao#findByDateRange(java.util.Date, java.util.Date)
	 */
	public List<Price> findByDateRange(String label, Date start, Date end) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("start", start);
        params.put("end", end);
        params.put("label", label);
        
		String sql = "SELECT * FROM price WHERE label = :label AND date >= :start AND date <= :end ORDER BY date DESC";		
        List<Price> result = namedParameterJdbcTemplate.query(sql,params,new PriceMapper());
        
		return result;
	}

	/**
	 * PriceMapper maps the sql resultset to {@link Price} object
	 * @author Kaycee-PC
	 *
	 */
	private static final class PriceMapper implements RowMapper<Price> {

		public Price mapRow(ResultSet rs, int rowNum) throws SQLException {
			com.model.Price price = new Price();
			price.setDate(rs.getDate("date"));
			price.setPrice(rs.getBigDecimal("price"));
			return price;
		}
	}

}
