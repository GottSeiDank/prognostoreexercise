package com.model;

public class RestResponse {
	
	private boolean status;
	private Object data;
	private String message;
	
	public static final int SUCESSRESPONSE = 00;
	public static final int FAILEDRESPONSE = 99;
	
	public RestResponse(boolean status, Object data, String message) {
		super();
		this.status = status;
		this.data = data;
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	

}
