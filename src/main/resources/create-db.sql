CREATE TABLE price (
  price DECIMAL(10,2),
  date  DATE,
  label VARCHAR(50)
);